// Introduction to NodeJS

let http = require("http");
	// modules need to be imported or required

console.log(http);
	// http module contains methods and other codes which allowed us to create server and let our client communicate through HTTP

http.createServer((req, res) => {

	if(req.url === "/"){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Hi! Welcome to our Homepage')
	}else if(req.url === "/login"){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Hi! Welcome to the login page!')
	} else {
		res.writeHead(404, {'Content-Type' : 'text/plain'})
		res.end('Resource cannot be found')
	}
}).listen(4000);
console.log('Server is running on localhost: 4000');